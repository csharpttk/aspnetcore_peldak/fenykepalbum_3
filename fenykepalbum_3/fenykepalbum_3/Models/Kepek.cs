﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace fenykepalbum_3.AspNetCore.NewDb.Models
{
    public class fenykepContext : DbContext
    {
        public fenykepContext(DbContextOptions<fenykepContext> options)
            : base(options)
        { }
        public DbSet<Kepek>Kepek { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Kepek>().HasData(new Kepek
            {
                Id = 1,
                Filenev = "WP_20150531_15_50_32_Pro.jpg",
                Holkeszult = "Lednice",
                Mikor = new DateTime(2015, 5, 31),
                Leiras = "Kastély park, tó"

            }, new Kepek
            {
                Id=2,
                Filenev = "WP_20150531_16_01_58_Pro.jpg",
                Holkeszult = "Lednice",
                Mikor = new DateTime(2015, 5, 31),
                Leiras = "Kastély park, tó"
            }
            ); ;
        }

    }

    public class Kepek
    {
        public int Id { get; set; }
        public string Filenev { get; set; }
        public string Holkeszult { get; set; }
        public DateTime Mikor { get; set; }
        public string Leiras { get; set; }

     }
}